module.exports = {

  /**
   * where the content is stored
   * you can access this property from `bp.dataLocation`
   */
  dataDir: process.env.BOTPRESS_DATA_DIR || './data',

  modulesConfigDir: process.env.BOTPRESS_CONFIG_DIR || './modules_config',
  disableFileLogs: false,
  port: process.env.BOTPRESS_PORT || process.env.PORT || 3000,
  optOutStats: false,
  notification: {
    file: 'notifications.json',
    maxLength: 50
  },
  log: {
    file: 'bot.log',
    maxSize: 1e6 // 1mb
  },

  /**
   * Access control of admin panel
   */
  login: {
    enabled: process.env.NODE_ENV === 'production',
    tokenExpiry: '6 hours',
    password: process.env.BOTPRESS_PASSWORD || 'password',
    maxAttempts: 3,
    resetAfter: 5 * 60 * 10000 // 5 minutes
  },

  /**
   * Postgres configuration
   */
  postgres: {
    enabled: process.env.DATABASE === 'postgres',
    host: process.env.PG_HOST || '127.0.0.1',
    port: process.env.PG_PORT || 5432,
    user: process.env.PG_USER || '',
    password: process.env.PG_PASSWORD || '',
    database: process.env.PG_DB || '',
    ssl: process.env.PG_SSL || false
  },

  "botpress-messenger": {
    "applicationID": "1606160556079663",
    "accessToken": "EAAW0y2Vlui8BAI7T1uZAAXbAjRumxLSoah7Pau2Kp8IGuYexVafvS5pOa6F5oZCpTBRNrZC3BnzZCX3s1z22tqzO4RPoVXLculp7G6N6yQZCCWephJgPJFIBWMPwHEzVrPm7GFRmF6BphrubFEfetW1FDW0ClBCD5ZAW2LySWbYwZDZD",
    "appSecret": "6cc17fa41da7ac69f0eba4e669674ff4",
    "verifyToken": "22843b11-f952-4299-8b54-cd793d512e0a",
    "validated": false,
    "connected": true,
    "homepage": "https://github.com/botpress/botpress-messenger",
    "ngrok": true,
    "displayGetStarted": false,
    "greetingMessage": "Default greeting message",
    "persistentMenu": false,
    "persistentMenuItems": [],
    "automaticallyMarkAsRead": true,
    "targetAudience": "openToAll",
    "targetAudienceOpenToSome": "",
    "targetAudienceCloseToSome": "",
    "trustedDomains": [],
    "autoRespondGetStarted": true
  }


}
