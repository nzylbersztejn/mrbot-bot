bp.messenger.sendText(userId, "Hello, Nathan here from Mr. Bot. Here 7 recent articles if found particularly interesting.", {
  "waitDelivery": true
}).then(() => bp.messenger.sendTemplate(userId, {
  "template_type": "generic",
  "elements": [{
    "title": "How Chatbots, Blockchain Will Converge And Shape Music's Future",
    "image_url": "http://www.hypebot.com/.a/6a00d83451b36c69e201bb09adba2f970d-600wi",
    "subtitle": "In this article, Bas Grasmayer explores the significance of two developing technologies, chatbots and block, and how these two avenues of technological advancement, and their convergence, will serve to shape the future of the music industry. __________________________________ Guest post by Bas Grasmayer on Synchblog I want to make this Projecting...",
    "default_action": {
      "type": "web_url",
      "url": "http://www.hypebot.com/hypebot/2017/07/how-chatbots-blockchain-will-converge-and-shape-musics-future.html#sthash.SseLskkf.uxfs",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "http://www.hypebot.com/hypebot/2017/07/how-chatbots-blockchain-will-converge-and-shape-musics-future.html#sthash.SseLskkf.uxfs",
      "title": "Read"
    }]
  }, {
    "title": "Amazon’s Alexa Accelerator announces its first 13-week startup class",
    "image_url": "https://venturebeat.com/wp-content/uploads/2017/06/download-11.png?fit=780%2C439&strip=all",
    "subtitle": "Amazon’s Alexa Accelerator today announced the names of nine startups chosen to participate in its inaugural session. The 13-week accelerator program, which will be managed by Techstars, will…",
    "default_action": {
      "type": "web_url",
      "url": "https://venturebeat.com/2017/07/17/amazons-alexa-accelerator-announces-its-first-13-week-startup-class/",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://venturebeat.com/2017/07/17/amazons-alexa-accelerator-announces-its-first-13-week-startup-class/",
      "title": "Read"
    }]
  }, {
    "title": "5 ways chatbots need to evolve before they go mainstream",
    "image_url": "https://venturebeat.com/wp-content/uploads/2017/06/shutterstock_664063189.jpg?fit=780%2C520&strip=all",
    "subtitle": "It’s hard to swing a $400 juicer in Silicon Valley these days without hitting a chatbot. Advances in artificial intelligence have enabled these talkative assistants to become a reality, and they’re…",
    "default_action": {
      "type": "web_url",
      "url": "https://venturebeat.com/2017/07/15/5-ways-chatbots-need-to-evolve-before-they-go-mainstream/",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://venturebeat.com/2017/07/15/5-ways-chatbots-need-to-evolve-before-they-go-mainstream/",
      "title": "Read"
    }]
  }, {
    "title": "The Language of Chatbots – Chatbots Magazine",
    "image_url": "https://cdn-images-1.medium.com/max/1200/0*rGaJt76QUCA0RByT.",
    "subtitle": "At Dashbot, we recently surpassed 7 billion messages processed.",
    "default_action": {
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/the-language-of-chatbots-99a38be54b58?source=rss----d6dc2c824f17---4&gi=c73cd5463d54",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/the-language-of-chatbots-99a38be54b58?source=rss----d6dc2c824f17---4&gi=c73cd5463d54",
      "title": "Read"
    }]
  }, {
    "title": "Chatbot Acquisition & Engagement Strategies – Chatbots Magazine",
    "image_url": "https://cdn-images-1.medium.com/max/1200/1*j1TylkSvVpzC1OT9GiERDg.jpeg",
    "subtitle": "A conversation with the bot leaders at Reply.ai, TOPBOTS, Kip and Wade & Wendy",
    "default_action": {
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/chatbot-acquisition-engagement-strategies-682f9bcf70ce",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/chatbot-acquisition-engagement-strategies-682f9bcf70ce",
      "title": "Read"
    }]
  }, {
    "title": "What Slack Can Teach Us About Privacy In Enterprise Blockchains",
    "image_url": "https://gendal.files.wordpress.com/2017/07/slack.png",
    "subtitle": "“Channels” in Hyperledger Fabric don’t work the way you think they do… Corda and Fabric have very different approaches to delivering privacy. In this post, I compare the models, explain why Corda w…",
    "default_action": {
      "type": "web_url",
      "url": "https://gendal.me/2017/07/20/what-slack-can-teach-us-about-privacy-in-enterprise-blockchains/",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://gendal.me/2017/07/20/what-slack-can-teach-us-about-privacy-in-enterprise-blockchains/",
      "title": "Read"
    }]
  }, {
    "title": "Hipmunk in a Loop and the Three Rules of ChatBot Design",
    "image_url": "https://cdn-images-1.medium.com/max/1200/1*x-l7MXD2eJC3N_L4_SwNpw.png",
    "subtitle": "I love The Hipmunk. And who wouldn’t? Their agony score, ranking of flight by level of inconvenience, have saved me from many crazy red…",
    "default_action": {
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/hipmunk-in-a-loop-and-the-three-rules-of-chatbot-design-dcb5f8fe26cd?source=rss----d6dc2c824f17---4&gi=6ed954f2e3c0",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://chatbotsmagazine.com/hipmunk-in-a-loop-and-the-three-rules-of-chatbot-design-dcb5f8fe26cd?source=rss----d6dc2c824f17---4&gi=6ed954f2e3c0",
      "title": "Read"
    }]
  }, {
    "title": "Looking to the future of apps in Slack – Slack Platform Blog – Medium",
    "image_url": "https://cdn-images-1.medium.com/max/1200/1*hhe_EeNP8vUDZm-cuefeEQ.png",
    "subtitle": "Improving the way Slack apps are built, installed and distributed",
    "default_action": {
      "type": "web_url",
      "url": "https://medium.com/slack-developer-blog/looking-to-the-future-of-apps-in-slack-c2633df0bcb7",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://medium.com/slack-developer-blog/looking-to-the-future-of-apps-in-slack-c2633df0bcb7",
      "title": "Read"
    }]
  }, {
    "title": "5 Keys of a Good Chatbot – Csenger Szabo – Medium",
    "image_url": "https://cdn-images-1.medium.com/max/1200/1*s0pogbftUaRkBJeCW3-cnA.png",
    "subtitle": "The most important things you have to keep in mind when developing a chatbot",
    "default_action": {
      "type": "web_url",
      "url": "https://medium.com/@csengerszab/6-keys-of-a-good-chatbot-b9f6be073e13",
      "webview_height_ratio": "tall"
    },
    "buttons": [{
      "type": "web_url",
      "url": "https://medium.com/@csengerszab/6-keys-of-a-good-chatbot-b9f6be073e13",
      "title": "Read"
    }]
  }]
}, {
  "waitDelivery": true
})).then(() => bp.messenger.sendText(userId, "Feedback? question? Project? Always happy to hear from you :) Just ask bot for a human or tap 'Get in touch' in the menu."), {
  "waitDelivery": true
})