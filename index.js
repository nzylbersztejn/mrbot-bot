const BotpressBot = require('botpress-botkit').BotpressBot;
const _ = require('lodash');
const Promise = require("bluebird");
module.exports = function (bp) {

  bp.middlewares.load();

  let defaultOptions = {
    typing:true,
    waitDelivery:true
  };

  bp.hear({ platform: 'facebook', type:'postback', text: 'INTRO' }, event => {
    bp.messenger.sendText(event.user.id, 'Hi '+ event.user.first_name +'!', defaultOptions);
    bp.messenger.sendText(event.user.id, 'I\'m very excited to be connected with you :)', defaultOptions);
    bp.messenger.sendText(event.user.id, 'I guess you\'re as excited as me about bots, right?', defaultOptions);
    bp.messenger.sendText(event.user.id, 'So from now on I\'ll send you a weekly digest with everything you need to know about bots. How does that sound?', {
      typing:true,
      quick_replies: [
        {
          content_type :"text",
          title: "That sounds great!",
          payload: "SUBSCRIBE_WEEKLY"
        },
        {
          content_type :"text",
          title: "I'm not sure",
          payload: "DIGEST_NOT_SURE"
        }
      ]});
  });

  bp.hear({ platform: 'facebook', text: 'DIGEST_NOT_SURE' }, (event) => {
    bp.messenger.sendText(event.user.id, "It's just a great compilation of good reads about chatbots.",defaultOptions);
    bp.messenger.sendText(event.user.id, "One message per week, and you can unsusbscibe anytime from the menu.", {
      typing:true,
      waitDelivery:true,
      quick_replies: [
        {
          content_type :"text",
          title: "Yes, I'm in!",
          payload: "SUBSCRIBE_WEEKLY"
        },
        {
          content_type :"text",
          title: "Not today, thanks",
          payload: "DIGEST_REFUSED"
        }
      ]});
  });

  bp.hear({ platform: 'facebook', text: 'DIGEST_REFUSED' }, (event) => {
    bp.messenger.sendText(event.user.id, "Hmmm. I thought I was more convincing :)", defaultOptions);
    bp.messenger.sendText(event.user.id, "Anyways, you can always subscribe from the menu. Feel free to chat anytime. ", defaultOptions);
  });

  // match an intent to talk to a real human
  function askHuman(event, responseText) {
    bp.messenger.sendText(event.user.id, responseText, {
      typing: true,
      quick_replies: [
        {
          content_type: "text",
          title: "Yes please!",
          payload: "WORDHOP"
        },
        {
          content_type: "text",
          title: "Not right now",
          payload: "NO_WORDHOP"
        }
      ]
    });
  }

  bp.hear({text: /((^|, )(human|Human|nathan|Nathan|NATHAN|help|HELP|GET_IN_TOUCH))+$/ }, (event,next) => {
    // let the user know that they are being routed to a human
    var responseText = 'I can connect you with Nathan, he\'s always happy to help, discuss ideas and projects. Would you like that?';
    askHuman(event, responseText);
  });

  bp.hear({ platform: 'facebook', text: 'WORDHOP' }, (event,next) => {
    bp.messenger.sendText(event.user.id, "Hang tight, a human is on the way :)", defaultOptions);
    bp.events.emit('assistanceRequested', {platform: event.platform, raw: event.raw})
  });

  bp.hear({ platform: 'facebook', text: 'NO_WORDHOP' }, (event,next) => {
    bp.messenger.sendText(event.user.id, "Ok, feel free to get in touch anytime :)", defaultOptions);
  });

  bp.hear({'nlp.action': /smalltalk/i }, (event,next) => {
    bp.messenger.sendText(event.user.id, event.nlp.fulfillment.speech)
  });

  bp.hear({'nlp.action': "input.unknown" }, (event,next) => {
    bp.messenger.sendText(event.user.id, event.nlp.fulfillment.speech)
    askHuman(event,"Do you want me to connect with a human?")
  });

  bp.hear({'nlp.action': "input.whoareyou" }, (event,next) => {
    event.nlp.fulfillment.messages.map(m => bp.messenger.sendText(event.user.id,m,defaultOptions));
    var responseText = 'I can connect you with Nathan, he\'s always happy to help, discuss ideas and projects. Would you like that?';
    askHuman(event, responseText);
  });
};