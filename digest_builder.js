/**
 * Created by nathanzylbersztejn on 17-04-12.
 */


const ogs = require('open-graph-scraper');
const sprintf = require('sprintf-js').sprintf;
const beautify = require('js-beautify').js_beautify;
const fs = require('fs');

const intro = "Hello, Nathan here from Mr. Bot. Here 7 recent articles if found particularly interesting.";
const outro = "Feedback? question? Project? Always happy to hear from you :) Just ask bot for a human or tap 'Get in touch' in the menu.";

let urls = [
  {
    url:"http://www.hypebot.com/hypebot/2017/07/how-chatbots-blockchain-will-converge-and-shape-musics-future.html#sthash.SseLskkf.uxfs"
  },
  {
    url:"https://venturebeat.com/2017/07/17/amazons-alexa-accelerator-announces-its-first-13-week-startup-class/"
  },
  {
    url:"https://venturebeat.com/2017/07/15/5-ways-chatbots-need-to-evolve-before-they-go-mainstream/"
  },
  {
    url:"https://chatbotsmagazine.com/the-language-of-chatbots-99a38be54b58?source=rss----d6dc2c824f17---4&gi=c73cd5463d54",
  },
  {
    url:"https://chatbotsmagazine.com/chatbot-acquisition-engagement-strategies-682f9bcf70ce"
  },
  {
    url:'https://gendal.me/2017/07/20/what-slack-can-teach-us-about-privacy-in-enterprise-blockchains/'
  },
  {
    url:'https://chatbotsmagazine.com/hipmunk-in-a-loop-and-the-three-rules-of-chatbot-design-dcb5f8fe26cd?source=rss----d6dc2c824f17---4&gi=6ed954f2e3c0'
  },
  {
    url:"https://medium.com/slack-developer-blog/looking-to-the-future-of-apps-in-slack-c2633df0bcb7"
  },
  {
    url:'https://medium.com/@csengerszab/6-keys-of-a-good-chatbot-b9f6be073e13'
  }

];

function promiseOgs(item) {
  // console.log(item)

  return new Promise((resolve, reject)=> {
    ogs({url: item.url}, function (err, results) {
      if (err)
        return reject(err);
      else {
        resolve({
            title: results.data.ogTitle,
            image_url: results.data.ogImage.url,
            subtitle: item.description ? item.description : results.data.ogDescription,
            default_action: {
              type: "web_url",
              url: item.url,
              webview_height_ratio: "tall"
            },
            buttons: [
              {
                type: "web_url",
                url: item.url,
                title: "Read"
              }
            ]
          }
        );
      }
    });
  })
}


const promises = urls.map(promiseOgs);
Promise.all(promises)
  .then(r=> {

      const message = {
        template_type: "generic",
        elements: r
      };

      const opts = JSON.stringify({waitDelivery: true});
      let seq = sprintf("bp.messenger.sendText(userId, \"%s\", %s)", intro, opts);
      seq += sprintf(".then(()=>%s)", sprintf("bp.messenger.sendTemplate(userId, %s, %s)", JSON.stringify(message), opts));
      seq += sprintf(".then(()=>%s)", sprintf("bp.messenger.sendText(userId,\"%s\"),%s", outro, opts));
      fs.writeFile("broadcast.js", beautify(seq, {indent_size: 2}), function (err) {
        if (err) {
          return console.log(err);
        }

        console.log("The file was saved!");
      });
    }
  );